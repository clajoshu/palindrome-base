package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean test = Palindrome.isPalindrome("racecar");
		assertTrue("This is not a palindrome", test);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean test = Palindrome.isPalindrome("notapalindrome");
		assertFalse("This is not a palindrome", test);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean test = Palindrome.isPalindrome("taco cat");
		assertTrue("This is not a palindrome", test);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean test = Palindrome.isPalindrome("Race on car");
		assertFalse("This is not a palindrome", test);
	}	
	
}
